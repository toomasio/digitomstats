﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomStats
{
    public class Respawnable : MonoBehaviour
    {
        [SerializeField] private float respawnDelay;

        private Vector3 lastCheckpointPos;
        private Coroutine respawnRoutine;
        private bool respawning;

        public event Action OnRespawn;

        public void SaveCheckPointPosition(Vector3 position)
        {
            lastCheckpointPos = position;
        }

        public void Respawn()
        {
            if (respawning) return;
            if (respawnRoutine != null)
                StopCoroutine(respawnRoutine);
            respawnRoutine = StartCoroutine(StartRespawn());
        }

        IEnumerator StartRespawn()
        {
            respawning = true;
            float timer = 0;
            while (timer < respawnDelay)
            {
                timer += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            DoRespawn();
            respawning = false;
        }

        void DoRespawn()
        {
            transform.position = lastCheckpointPos;
            OnRespawn?.Invoke();
        }
    }
}