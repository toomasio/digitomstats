﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace DigitomStats
{
    public class StatComponentUI : MonoBehaviour
    {
        [SerializeField] private Text text;

        public void UpdateUI(float amount)
        {
            text.text = amount.ToString();
        }
    }
}