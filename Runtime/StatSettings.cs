﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomStats 
{
    [System.Serializable]
    public class StatSettings 
    {
        public float minAmount;
        public float maxAmount;
        public float defaultAmount;
    }

}