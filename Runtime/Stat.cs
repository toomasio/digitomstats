﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomStats
{
    public class Stat
    {
        private readonly StatSettings settings;
        public float CurAmount{get; private set;}

        public event System.Action<float> OnChanged;
        public event System.Action<float> OnEmpty;
        public event System.Action<float> OnFull;
        public event System.Action<float> OnDecrease;
        public event System.Action<float> OnIncrease;

        public Stat(StatSettings settings)
        {
            this.settings = settings;
            CurAmount = settings.defaultAmount;
            CheckAmount();
        }

        public void SetAmount(float amount)
        {
            CurAmount = amount;
            CheckAmount();
        }

        public void SetToDefault()
        {
            CurAmount = settings.defaultAmount;
            CheckAmount();
        }

        public void Decrease(float amount)
        {
            CurAmount -= amount;
            OnDecrease.Invoke(amount);
            CheckAmount();
        }

        public void Increase(float amount)
        {
            CurAmount += amount;
            OnIncrease.Invoke(amount);
            CheckAmount();
        }

        void CheckAmount()
        {
            CurAmount = Mathf.Clamp(CurAmount, settings.minAmount, settings.maxAmount);
            if (CurAmount == settings.maxAmount)
                OnFull?.Invoke(CurAmount);
            else if(CurAmount == settings.minAmount)
                OnEmpty?.Invoke(CurAmount);
            OnChanged?.Invoke(CurAmount);
        }
    }
}