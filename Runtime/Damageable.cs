﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomStats
{
    public class Damageable : StatComponent
    {
        public enum KillType { None, Destroy, Respawn }
        [SerializeField] private KillType killType;
        [SerializeField] private float destroyDelay;
        [SerializeField] private Respawnable respawnable;
        [SerializeField] private bool resetHealthOnRespawn;
        
        public virtual void Kill()
        {
            if (killType == KillType.Destroy)
                Destroy(gameObject, destroyDelay);
            else if (killType == KillType.Respawn)
            {
                DoRespawn();
            }
                
        }

        protected override void OnEmpty(float amount)
        {
            base.OnEmpty(amount);
            Kill();
        }

        void DoRespawn()
        {
            if (!respawnable) return;

            respawnable.Respawn();
        }

        protected override void SubscribeStatEvents()
        {
            base.SubscribeStatEvents();
            if (killType == KillType.Respawn && respawnable && resetHealthOnRespawn)
                respawnable.OnRespawn += ResetStatsToDefault;
        }

        protected override void UnSubscribeStatEvents()
        {
            base.UnSubscribeStatEvents();
            if (killType == KillType.Respawn && respawnable && resetHealthOnRespawn)
                respawnable.OnRespawn -= ResetStatsToDefault;
        }
    }
}