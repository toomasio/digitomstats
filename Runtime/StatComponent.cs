﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DigitomStats
{
    public abstract class StatComponent : MonoBehaviour
    {
        [SerializeField] private StatComponentSettings statSettings;
        [SerializeField] private StatComponentUI ui;
        public StatComponentSettings Stat { get { return statSettings; } }

        protected virtual void Awake()
        {
            InitializeStats();
        }

        protected virtual void OnEnable()
        {
            SubscribeStatEvents();
        }

        protected virtual void OnDisable()
        {
            UnSubscribeStatEvents();
        }

        public virtual void Increase(float amount)
        {
            statSettings.Increase(amount);
        }

        public virtual void Decrease(float amount)
        {
            statSettings.Decrease(amount);
        }

        protected virtual void ResetStatsToDefault()
        {
            statSettings.SetToDefault();
        }

        protected virtual void InitializeStats()
        {
            statSettings.Initialize();
            if (ui) ui.UpdateUI(statSettings.stat.CurAmount);
        }

        protected virtual void SubscribeStatEvents()
        {
            statSettings.SubscribeEvents();
            statSettings.stat.OnEmpty += OnEmpty;
            statSettings.stat.OnFull += OnFull;
            if (ui) statSettings.stat.OnChanged += ui.UpdateUI;
        }

        protected virtual void UnSubscribeStatEvents()
        {
            statSettings.UnSubscribeEvents();
            statSettings.stat.OnEmpty -= OnEmpty;
            statSettings.stat.OnFull -= OnFull;
            if (ui) statSettings.stat.OnChanged -= ui.UpdateUI;
        }

        protected virtual void OnEmpty(float amount)
        {

        }

        protected virtual void OnFull(float amount)
        {

        }


    }
}