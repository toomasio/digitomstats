﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomEvents;

namespace DigitomStats
{
    public class GOEDamage : GameObjectEvent
    {
        [SerializeField] private float damageAmount;

        protected override void DoAffectedObjectEvent(GameObject affectedObject)
        {
            var dam = affectedObject.GetComponent<Damageable>();
            if (dam)
                dam.Decrease(damageAmount);
        }
    }
}


