﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomEvents;

namespace DigitomStats
{
    public class GOESetRespawnCheckpoint : GameObjectEvent
    {
        [SerializeField] private Transform position;
        [SerializeField] private bool singleUse;

        private bool used;

        protected override void DoAffectedObjectEvent(GameObject affectedObject)
        {
            if (singleUse && used) return;
            var res = affectedObject.GetComponent<Respawnable>();
            if (res) res.SaveCheckPointPosition(position.position);
            used = true;
        }
    }
}


