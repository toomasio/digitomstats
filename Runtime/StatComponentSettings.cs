﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace DigitomStats
{
    [System.Serializable]
    public class StatComponentSettings
    {
        [SerializeField] private StatSettings settings = null;
        [SerializeField] private UnityEvent onIncrease;
        [SerializeField] private UnityEvent onDecrease;
        [SerializeField] private UnityEvent onEmpty;
        [SerializeField] private UnityEvent onFull;
        public Stat stat { get; private set; }

        public virtual void Initialize()
        {
            stat = new Stat(settings);
        }

        public virtual void SubscribeEvents()
        {
            stat.OnIncrease += OnIncrease;
            stat.OnDecrease += OnDecrease;
            stat.OnFull += OnFull;
            stat.OnEmpty += OnEmpty;
        }

        public virtual void UnSubscribeEvents()
        {
            stat.OnIncrease -= OnIncrease;
            stat.OnDecrease -= OnDecrease;
            stat.OnFull -= OnFull;
            stat.OnEmpty -= OnEmpty;
        }

        public void SetAmount(float amount)
        {
            stat.SetAmount(amount);
        }

        public void SetToDefault()
        {
            stat.SetToDefault();
        }

        public void Decrease(float amount)
        {
            stat.Decrease(amount);
        }

        public void Increase(float amount)
        {
            stat.Increase(amount);
        }

        protected virtual void OnIncrease(float amount)
        {
            onIncrease.Invoke();
        }

        protected virtual void OnDecrease(float amount)
        {
            onDecrease.Invoke();
        }

        protected virtual void OnFull(float amount)
        {
            onFull.Invoke();
        }

        protected virtual void OnEmpty(float amount)
        {
            onEmpty.Invoke();
        }
    }
}